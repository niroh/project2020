// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    firebaseConfig : {
      apiKey: "AIzaSyA5w1vbpg__KuD7rMaKcFlzwabFLQ1dX58",
      authDomain: "project2020-9a9d1.firebaseapp.com",
      databaseURL: "https://project2020-9a9d1.firebaseio.com",
      projectId: "project2020-9a9d1",
      storageBucket: "project2020-9a9d1.appspot.com",
      messagingSenderId: "1026683354207",
      appId: "1:1026683354207:web:86dd40135d931497452fea"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
