  import { Injectable } from '@angular/core';
  import { Observable } from 'rxjs';
  import { AngularFireAuth } from '@angular/fire/auth';
  import { ActivatedRoute, Router } from '@angular/router';
  import { User } from './interfaces/user';

  @Injectable({
    providedIn: 'root'
  })
  export class AuthService {

    constructor(public afAuth:AngularFireAuth,
                public router:Router,
                public route:ActivatedRoute) {
            this.user = this.afAuth.authState;
            }

  user: Observable<User | null>
  public err:any;  

  signup(email:string, password:string){
  this.afAuth
  .auth
  .createUserWithEmailAndPassword(email,password)
  .then(res =>{
            console.log('Succesful sign up',res);
            this.router.navigate(['/moviesapi']);
            })
            .catch(
            error => {this.err = error})  
  }

  login(email:string, password:string){
  this.afAuth
  .auth.signInWithEmailAndPassword(email,password)
  .then(
  res =>  
    {
      console.log('Succesful Login',res);
      this.router.navigate(['/moviesapi']);
    })
  .catch (
  error => {this.err = error
  }) 
  }


  logout(){
  this.afAuth.auth.signOut().then(res => console.log('Logged Out', this.router.navigate(['/moviesapi'])));
  }

  }