import { Component, OnInit } from '@angular/core';
import { RecService } from '../services/rec.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-recform',
  templateUrl: './recform.component.html',
  styleUrls: ['./recform.component.css']
})
export class RecformComponent implements OnInit {

  constructor(public recservice:RecService,
              public authservice:AuthService,
              public router:Router,
              private route:ActivatedRoute) { }

  name:string;
  genres:object[]=[{value:'Action'},{value:'Adventure'},{value:'Comedy'},{value:'Drama'},{value:'Horror'}]
  text:string;
  link:string;
  genreInput:string;
  userId:string;
  id:string;
  isEdit:boolean = false;
  userEmail:string;
  buttonText:string = "Add New Recommendation"


  ngOnInit() {
    this.id = this.route.snapshot.params.id; 

    this.authservice.user.subscribe(
      user=> {
        this.userId = user.uid;
        this.userEmail = user.email;
    if(this.id){
      this.isEdit = true;
      this.buttonText = "Update Recommendation";
      this.recservice.getRec(this.id).subscribe(
        rec => {
          this.name = rec.data().name;
          this.genreInput = rec.data().genreInput;
          this.text = rec.data().text;
          this.link = rec.data().link;
        })
    }
      }
    )
    
  }

  onSubmit(){
    if(this.isEdit){
      this.recservice.updateRec(this.id, this.name, this.genreInput, this.text, this.link)
    }
    else{
    this.recservice.addRec(this.userId,this.name, this.genreInput, this.text, this.link,this.userEmail)
  }
    this.router.navigate(['/myrec']);

  }

}
