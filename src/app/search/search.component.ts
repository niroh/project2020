import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClassifyService } from '../services/classify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private classifyservice:ClassifyService,
    private router:Router) { }
    text:string;


  ngOnInit() {
  }

  
  onSubmit(){
    this.classifyservice.doc = this.text;
    this.router.navigate(['/classified']);
  }


}
