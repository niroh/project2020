import { MoviesRaw } from './../interfaces/movies-raw';
import { Movie } from './../interfaces/movie';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MoviesapiService {

  constructor(public http:HttpClient) { }

  private moviesURL = "https://api.themoviedb.org/3/movie/22/recommendations?api_key=cab3d444f11529ae75dcb704c3fa15f5&language=en-US&page=1";
  private imageURL = "http://image.tmdb.org/t/p/w154/"
  
  /*
  searchMoviesData(): Observable<Movie[]>{
    return this.http.get<MoviesRaw>(`${this.moviesURL}`)
    .pipe(map(data=>this.transformMovieData(data))
    )

  }

  private transformMovieData(data:MoviesRaw):Movie[]{
    var response:any;
    for(var i=0; i<data.results.length; i++)
      {
          response[i].id = data.results[i].id,
          response[i].voteCount = data.results[i].voteCount,
          response[i].vote = data.results[i].vote,
          response[i].title = data.results[i].title,
          response[i].overview = data.results[i].overview,
          response[i].image = `http://image.tmdb.org/t/p/w154/${data.results[i].image}`
      }
      return response;
  }
  */
  getMovies(){
    //console.log(this.http.get<Movie[]>(this.moviesURL))
    return this.http.get<Object>(this.moviesURL);

  }




}
