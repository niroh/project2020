import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { firestore } from 'firebase';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class RecService {

  constructor(public db:AngularFirestore,
              public authservice:AuthService,
              private router:Router) 
              { }

  userCollection: AngularFirestoreCollection = this.db.collection('users');
  recCollection: AngularFirestoreCollection
  likes:any
  userId: string;
  id:string;
/*
  config:any = {
          apiKey: "AIzaSyA5w1vbpg__KuD7rMaKcFlzwabFLQ1dX58",
          authDomain: "project2020-9a9d1.firebaseapp.com",
          projectId: "project2020-9a9d1"
      };
       app = firebase.initializeApp(firebaseConfig);
       a = firebase.firestore(this.app);
      //firebase.firestore.setLogLevel("debug");
 */
 

  addRec(userId:string, name:string, genreInput:string, text:string, link:string, userName:string){
    //const rec = {name:name, genreInput:genreInput, text:text, link:link}
    const recUser= {userId:userId, name:name, genreInput:genreInput, text:text, link:link, userName:userName, likes:0}
    this.db.collection('recommendations').add(recUser);
   // this.userCollection.doc(userId).collection('recommendations').add(rec);
  }

  updateLike(id:string,likes:string){
    this.db.doc(`/recommendations/${id}`).update({
      likes:likes,
      })
  }

  getMyRec(userId:string):Observable<any[]>{
    //return this.db.collection('books').valueChanges({idField:'id'});
    this.recCollection = this.db.collection(`users/${userId}/recommendations`);
    return this.recCollection.snapshotChanges().pipe(
      map(
        collection => collection.map(
          document => {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            return data;
          }
        )
  
      )
    )
  }

  getAllRecs():Observable<any[]>{
    return this.db.collection('recommendations').valueChanges({idField:'id'});
        /*
    this.a.collection("users").where("recommendations", "==", true)
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
        });
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });
      //return this.db.collection(`users`).where
      // Get a reference to the restaurant

      //var restRef = this.db.collection('recommendations').doc(context.params.restId);
      /*const snapshot = await;
      this.db.ref('users/recommendations');
     const a = this.db.collection("users").get().toPromise().then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
            //return doc.data();
        });

    });  
    return a.snapshotChanges().pipe(
      map(
        collection => collection.map(
          document => {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            return data;
          }
        )
  
      )
    )*/
  }

  deleteRec(id:string){
   // this.db.doc(`users/${userId}/recommendations/${id}`).delete();
   this.db.doc(`recommendations/${id}`).delete();
   //const allRec = this.db.collection('recommendations')
    };
  
    updateRec(id:string, name:string, genreInput:string, text:string, link:string){
    this.db.doc(`/recommendations/${id}`).update({
      name:name,
      genreInput:genreInput,
      text:text,
      link:link
    })
  }
  
  getRec(id:string):Observable<any>{
    return this.db.doc(`recommendations/${id}`).get()
  }
   
}
