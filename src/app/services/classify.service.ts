import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  constructor(private http:HttpClient,
              public db:AngularFirestore) { }

  private url = "https://kj9j8wj9y1.execute-api.us-east-1.amazonaws.com/beta";

  public categories:object = {'1': '__lable__Drama', '2': '__lable__Adventure', '3': '__lable__Comedy', '4': '__lable__Horror', '5': '__lable__Action'};
  public prob:any;
  public doc:string;
  public body:any;
  public text:any;

  classify():Observable<any>{
    this.text = this.doc;
    let json = '" '+ this.doc +'"'
    this.body = JSON.stringify(json);


    return this.http.post<any>(this.url,this.body).pipe(
      map(res => {
        let final = res.body;
        console.log(final)

        return final;
      })
    )
  }

    

}
