import { AuthService } from './../auth.service';
import { SavedService } from './../services/saved.service';
import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../services/classify.service';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  constructor(public classifyservice:ClassifyService,
              public savedservice:SavedService,
              public authservice:AuthService
              //public imageservice:ImageService
              ) { }

category:any;
label:string;
body:string;
userId:string;
manCat:string = 'No Change';
secondCategorylabel:any;
secondCategoryprob:any;
//categoryImage:string;

categorytypes:object[]=[{name: 'No Change'}, {name: 'Comedy'}, {name: 'Action'}, {name: 'Adventure'}, {name: 'Horror'}, {name: 'Drama'}];


  ngOnInit() {

    this.authservice.user.subscribe(
      user => {
        this.userId = user.uid;
      })

    this.classifyservice.classify().subscribe(
      res => {
        this.category = res;
        console.log(this.category)
        var catUpdate = this.category[0].label;
        var secondCatUpdate = this.category[0].label[1];
        this.body = this.classifyservice.text;
        console.log(secondCatUpdate)
        var x = catUpdate[0].split("__label__").pop();
        var x2 = secondCatUpdate.split("__label__").pop();
        this.category[0].label=x
        this.secondCategorylabel = x2
        var y=this.category[0].prob[0]
        var y2 = this.category[0].prob[1]
        y = y*100
        y = y.toFixed(3)
        y2 = y2*100
        y2 = y2.toFixed(3)
        this.category[0].prob = y
        this.secondCategoryprob = y2
        console.log(this.secondCategoryprob)
        console.log(this.secondCategorylabel)
       // console.log(y[0].toFixed(2))
        //console.log(this.category[0].label)
        //this.categoryImage = this.imageservice.images[res];
      }
    )

  }

  onClick(cat:string, prob:any){
    console.log(this.body)
    console.log(cat)
    console.log(prob)
    console.log(this.userId)
    console.log(this.manCat)
    this.savedservice.saveClass(this.body, cat, this.manCat, prob,  this.userId)
    
  }

}
