export interface Movie {
            id:number,
            voteCount:number,
            vote:string,
            title:string; 
            overview:string,
            image:string,
}

