export interface MoviesRaw {
    results:[
        {
            id:number,
            voteCount:number,
            vote:string,
            title:string; 
            overview:string,
            image:string
        }
    ];
}