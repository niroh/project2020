export interface Recs {
    name:string,
    genreInput:string,
    text:string,
    link:string,
    userId:string
}
