import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SavedService } from '../services/saved.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-savedclass',
  templateUrl: './savedclass.component.html',
  styleUrls: ['./savedclass.component.css']
})
export class SavedclassComponent implements OnInit {

  constructor(public authservice:AuthService,
    public router:Router,
    public activatedroute:ActivatedRoute,
    public savedservice:SavedService) { }
    
email:string;
password:string;
userId:string;
  
savedClass$:Observable<any>;
  
ngOnInit() {
  // this.savedClass$ = this.savedservice.getSavedClass();
  this.authservice.user.subscribe(
    user => {
    this.userId = user.uid;
    this.email = user.email;
    console.log(this.userId);
    this.savedClass$ = this.savedservice.getSavedClass(this.userId);
    //console.log('there is '+this.savedClass$);

    }
  )
}

}
