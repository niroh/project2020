import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public authservice:AuthService,
    private router:Router,
    private route:ActivatedRoute) { }

email:string;
password:string;

onSubmit(){
this.authservice.login(this.email,this.password);
}


  ngOnInit() {
  }

}