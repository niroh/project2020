import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../auth.service';
import { RecService } from '../services/rec.service';
import { Observable } from 'rxjs';
import {MatTableDataSource} from '@angular/material/table';
import { Recs } from '../interfaces/recs';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(public authservice:AuthService,
              public recservice:RecService) { }

  recs$:Recs[];
  datasource: any;
  displayedColumns: string[] = ['No', 'Movie Name', 'Movie Genre', 'Recommendation','Username', 'IMDB Link', 'Likes'];
  counter:number=0;
  userId:string;
  email:string;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


              
  ngOnInit() { 
    this.authservice.user.subscribe(
      user => {
        this.userId = user.uid;
        this.email = user.email;
      })
   this.recservice.getAllRecs().subscribe(
     data => {
       this.recs$ = data;
       this.datasource = new MatTableDataSource<Recs>(this.recs$);
       this.datasource.paginator = this.paginator;
       this.datasource.sort = this.sort;
       this.counter = this.recs$.length;
       console.log(this.counter)
     });
    console.log(this.recs$)        
}

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.datasource.filter = filterValue.trim().toLowerCase();
  }

  addLikes(id:string, likes:any){
    likes++;
    this.recservice.updateLike(id,likes)
  }
}
