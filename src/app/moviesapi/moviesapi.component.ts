import { AuthService } from './../auth.service';
import { Movie } from './../interfaces/movie';
import { MoviesapiService } from './../services/moviesapi.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-moviesapi',
  templateUrl: './moviesapi.component.html',
  styleUrls: ['./moviesapi.component.css']
})
export class MoviesapiComponent implements OnInit {

  constructor(public moviesapiservice:MoviesapiService,
              public authservice:AuthService) {
              }
  image: string = "http://image.tmdb.org/t/p/w154/"
  movies$: Object;
  userId:string;
  email:string;
  results: any[]

  ngOnInit() {
  this.moviesapiservice.getMovies().subscribe(
    data => {
      this.results = data['results'];
      console.log(this.results)
    }
  );

  this.authservice.user.subscribe(
    user => {
      this.userId = user.uid;
      this.email = user.email;
    })
}


}
