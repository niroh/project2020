import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

//angular material
import { LayoutModule } from '@angular/cdk/layout';
import {CdkStepperModule} from '@angular/cdk/stepper';
import { MatPaginatorModule } from '@angular/material';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSliderModule } from '@angular/material/slider';
import {MatCardModule} from '@angular/material/card';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatStepperModule} from '@angular/material/stepper';


import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { WelcomeComponent } from './welcome/welcome.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { SearchComponent } from './search/search.component';
import { RecformComponent } from './recform/recform.component';
import { MyrecComponent } from './myrec/myrec.component';
import { SavedclassComponent } from './savedclass/savedclass.component';
import { MoviesapiComponent } from './moviesapi/moviesapi.component';
import { ClassifiedComponent } from './classified/classified.component';



const appRoutes: Routes = [
{ path: 'welcome', component: WelcomeComponent },
{ path: 'signup', component: SignupComponent },
{ path: 'login', component: LoginComponent },
{ path: 'search', component: SearchComponent },
{ path: 'saved', component: SavedclassComponent },
{ path: 'myrec', component: MyrecComponent },
{ path: 'recform', component: RecformComponent },
{ path: 'recform/:id', component: RecformComponent },
{ path: 'moviesapi', component: MoviesapiComponent },
{ path: 'classified', component: ClassifiedComponent },




  { path: "**",
    redirectTo: '/moviesapi',
    pathMatch: 'full'
  },
];


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    WelcomeComponent,
    SignupComponent,
    LoginComponent,
    SearchComponent,
    RecformComponent,
    MyrecComponent,
    SavedclassComponent,
    MoviesapiComponent,
    ClassifiedComponent,
    
  ],
  imports: [

    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireStorageModule,

    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),

    BrowserModule,
    AngularFirestoreModule,
    BrowserAnimationsModule,
    LayoutModule,
    CdkStepperModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatSliderModule,
    MatCardModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTableModule,
    MatCheckboxModule,
    AngularFireAuthModule,
    AngularFireModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    HttpClientModule

  ],
  providers: [
    AngularFireAuth,
  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
