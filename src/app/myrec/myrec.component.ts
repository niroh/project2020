import { Recs } from './../interfaces/recs';
import { User } from './../interfaces/user';
import { AuthService } from './../auth.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { RecService } from '../services/rec.service';
import { Observable } from 'rxjs';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';



@Component({
  selector: 'app-myrec',
  templateUrl: './myrec.component.html',
  styleUrls: ['./myrec.component.css']
})
export class MyrecComponent implements OnInit {

@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  constructor(public authservice:AuthService,
               public recservice:RecService) { }
  userId:string;
  ///recs$:Observable<any[]>;
  recs$:Recs[];
  datasource:any;
  id:string;
  displayedColumns: string[] = ['No','Movie Name', 'Movie Genre', 'Recommendation', 'IMDB Link', 'actions'];

  ngOnInit() {
    this.authservice.user.subscribe(
      user => {
        this.userId = user.uid;
      }
    )
     this.recservice.getAllRecs().subscribe(
        data => {
          this.recs$ = data;
          for(let i=0;i<this.recs$.length;i++){
            if(this.recs$[i].userId != this.userId){
              this.recs$.splice(i,1);
              i--;
            };
          }
          console.log(this.recs$)
          this.datasource = new MatTableDataSource<Recs>(this.recs$)
          this.datasource.paginator = this.paginator;

          //console.log(this.newLength);
        }
      );
     /* if(this.newLength != 0){
        console.log(this.recs$)
        for(let j=0;j<this.recs$.length;j++){
          if(this.recs$[j].userId != this.userId){
          }
        }
      }*/
      //console.log(this.newLength)
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.datasource.filter = filterValue.trim().toLowerCase();
  }



  deleteRecommand(id){
    console.log(id)
    this.recservice.deleteRec(id)
  }


}
